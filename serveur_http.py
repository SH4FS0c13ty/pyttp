# -*- coding: utf-8 -*-
#!/usr/bin/env python3
######################################################################
#                               PyTTP                                #
######################################################################
#          serveur_http.py is a part of the PyTTP project.           #
######################################################################
# serveur_http.py is used to start the Python HTTP webserver using   #
# the port submitted using arguments or default port (8000), it will #
# handle HTTP GET requests and reply with the requested resources or #
# an error. Requests shall be shown in the opened console.           #
######################################################################
# Shell syntax: serveur_http.py [-H HOST] [-p PORT] [-d DIRECTORY]   #
######################################################################
# Author: Adrien CL                                                  #
# Copyright: Copyright 2020, PyTTP                                   #
# Credits: [Adrien CL, E.Würbel]                                     #
# License: MIT License (https://opensource.org/licenses/MIT)         #
# Version: 1.0.0                                                     #
# Maintainer: Adrien CL                                              #
# Email: adriencl@outlook.fr                                         #
######################################################################

"""
This is the main module of the PyTTP (Python HTTP Webserver).
It can handle HTTP GET requests and accepts multiple clients connections.

:Authors: Adrien CL
"""

# System modules
import sys, os, socket, threading, signal

# Internal modules
import config_srv
import client_http


def check_ipv4(ip):
    """
    Check valid IPv4.

    :ip: IP to check
    :type ip: list
    :return: True or False
    :rtype: bool
    """

    # Check basic IPv4 requirements
    if type(ip) != list or len(ip) != 4:
        return False

    # Check each item for a valid IPv4 number
    for x in ip:
        try:
            x = int(x) # Converting string number to integer
            if x < 0 or x > 255:
                return False
        except ValueError:
            return False

    return True

def get_args(arguments):
    """
    Get the arguments and modify configuration.

    :arguments: Passed arguments when starting the webserver
    :type arguments: list
    :return: Nothing
    :rtype: None
    """

    # Check the arguments type, switches and values
    if type(arguments) != list:
        return None

    flagH = False # Flag for -H parameter
    flagp = False # Flag for -p parameter
    flagd = False # Flag for -d parameter

    for i in range(0, len(arguments)):
        if arguments[i] == "-H":
            if flagH != True:
                flagH = True # -H flag set to True because being used
                try:
                    hostarg = arguments[i+1].split(".") # Splitting parameter value (should be an IPv4 address)
                    res_check_ip = check_ipv4(hostarg) # Result of True or False IPv4 address
                    if res_check_ip == False:
                        print("Invalid host value, using 0.0.0.0 as default.")
                        config_srv.set_config("Hote", "0.0.0.0") # Use fallback address if wrong IPv4 address in CONFIGURATION
                    else:
                        config_srv.set_config("Hote", arguments[i+1]) # Use passed address in CONFIGURATION
                except IndexError:
                    print("Missing value after -H")
                except KeyboardInterrupt:
                    return None
                except Exception as e:
                    print(e)
        elif arguments[i] == "-p":
            if flagp != True:
                flagp = True # -p flag set to True because being used
                try:
                    portarg = int(arguments[i+1]) # Converting port string number to integer
                    config_srv.set_config("Port", portarg) # Use passed port in CONFIGURATION
                except IndexError:
                    print("Missing value after -p")
                except ValueError:
                    print("Port value error, using 8000 as default.")
                except KeyboardInterrupt:
                    return None
                except Exception as e:
                    print(e)
        elif arguments[i] == "-d":
            if flagd != True:
                flagd = True # -d flag set to True because being used
                try:
                    if os.path.isdir(arguments[i+1]) == False:
                        print("Invalid path, using \"html/\" as default.")
                    else:
                        config_srv.set_config("Rep_servi", arguments[i+1]) # Use passed HTML directory if exists
                except IndexError:
                    print("Missing value after -d")
                except TypeError:
                    print("Path value error, using \"html/\" as default.")
                except KeyboardInterrupt:
                    return None
                except Exception as e:
                    print(e)

    return None

def get_hostname():
    """ 
    Get hostname and put it in the configuration dictionary.

    :return: Nothing
    :rtype: None
    """

    # Get and write hostname ip address in CONFIGURATION
    config_srv.set_config("Hote", socket.gethostbyname(socket.gethostname()))

    return None

def print_config():
    """
    Print the current configuration.

    :return: Nothing
    :rtype: None
    """

    sep = "=================================================="
    print(sep)
    print("           Current PyTTP configuration")
    print(sep)
    print("Host:", config_srv.get_config("Hote"))
    print("Port:", config_srv.get_config("Port"))
    config_srv.lock.acquire()
    sections = config_srv.Config.sections()
    config_srv.lock.release()
    for conf_section in sections:
        print()
        print("Host:", conf_section)
        print("HTML directory:", "\"" + config_srv.get_config("Rep_servi", conf_section) + "\"")
    
    print(sep)

def server_stop(sock):
    """
    Stop the webserver.

    :sock: Current connected socket
    :type sock: socket.socket
    :return: Nothing
    :rtype: None
    """

    # Shutdown the webserver with a message
    print("The webserver is shutting down ...")
    try:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        sys.exit()
    except Exception:
        pass

    return None

def server_stop_keyboardinterrupt(signal, var):
    print("PyTTP is shutting down ...")
    sys.exit(1)

def ecoute(sock):
    """
    Listen on the passed socket and handle new connections.

    :sock: Socket to pass
    :type sock: socket.socket
    :return: Nothing
    :rtype: None
    """

    # Listen on the socket and accept connections
    try:
        sock.listen()
        while True:
            (client, addr) = sock.accept()
            print("Connection from", addr)
            lance(client, addr)
    except OSError:
        print("TCP communication error.")
        return None

    return None

def lance(sock_client, addr):
    myThread = threading.Thread(target=client_http.traite_client, args=(sock_client, addr))
    myThread.start()

def main():
    """ Main program """
    # Signal handler
    signal.signal(signal.SIGINT, server_stop_keyboardinterrupt)
    # Set and display configuration
    config_srv.lire_configuration()
    get_hostname()
    get_args(sys.argv)
    print_config()
    # Open the socket or stop the webserver if failure
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind((config_srv.get_config("Hote"), config_srv.get_config("Port")))
            print("The webserver started successfully.")
            ecoute(sock)
    except OSError as e:
        print("An error occured when opening the socket. Please check the detailed information below.")
        print(e)
    except PermissionError:
        print("Unauthorized port.")
        server_stop(sock)
        return None


if __name__ == "__main__":
    main()
