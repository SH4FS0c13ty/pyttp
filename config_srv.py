# -*- coding: utf-8 -*-
#!/usr/bin/env python3
######################################################################
#                               PyTTP                                #
######################################################################
#           config_srv.py is a part of the PyTTP project.            #
######################################################################
# config_srv.py is used as serveur_http.py default configuration.    #
######################################################################
# Shell syntax: This script has no shell syntax.                     #
######################################################################
# Author: Adrien CL                                                  #
# Copyright: Copyright 2020, PyTTP                                   #
# Credits: [Adrien CL, E.Würbel]                                     #
# License: MIT License (https://opensource.org/licenses/MIT)         #
# Version: 1.0.0                                                     #
# Maintainer: Adrien CL                                              #
# Email: adriencl@outlook.fr                                         #
######################################################################

"""
This is the config module of the PyTTP (Python HTTP Webserver).
It provides the necessary default configuration for PyTTP.

:Authors: Adrien CL
"""
# System modules
import os, threading, configparser

# Create configuration
pwd = os.getcwd()
# CONFIGURATION = {"Hote": "", "Port": 8000, "Rep_servi": pwd + "/html/"}
Config = configparser.ConfigParser()

# Create the lock variable
lock = threading.Lock()

def creer_config_defaut():
    """
    Create the default PyTTP configuration.

    :return: True if file has been properly written, False otherwise
    :rtype: bool
    """

    # Get the lock
    lock.acquire()

    # Create all sections and key/value combos
    sections = ["global"]
    keys = [{
        "Hote": "",
        "Port": "80",
        "Rep_servi": pwd + "/html/"
        }]
    for i in range(0, len(sections)):
        Config.add_section(sections[i])
        Config[sections[i]] = keys[i]

    # Write the config file
    try:
        with open(".serveur_http.conf", "w") as config_file:
            Config.write(config_file)
    except OSError:
        return False
    finally:
        lock.release()

    return True

def lire_configuration():
    """
    Read the configuration from the configuration file.

    :return: True if operations runned well, False otherwise
    :rtype: bool
    """

    config_file = ".serveur_http.conf"

    # Check if configuration file exists
    if not os.path.isfile(config_file):
        create_conf = creer_config_defaut()
        if not create_conf:
            return False
        else:
            return True

    # Read configuration from file
    lock.acquire()
    try:
        Config.read(config_file)
        if "Global" not in Config.sections():
            create_conf = creer_config_defaut()
            if not create_conf:
                return False
            else:
                return True
        else:
            return True
    except configparser.Error:
        create_conf = creer_config_defaut()
        if not create_conf:
            return False
        else:
            return True
    finally:
        lock.release()

    return True

def get_config(cle, host=None):
    """
    Get a configuration value according to a key.

    :cle: Key of the value
    :type cle: str
    :host: Hostname to check
    :type host: str
    :return: Value of the key
    :rtype: str or int or None
    """
    
    lock.acquire()

    if host is None:
        host = "Global"
    elif host == Config["Global"]["Hote"]:
        host = "Global"
    try:
        # value = CONFIGURATION[cle]
        value = Config[host].getint(str(cle))
    except ValueError:
        try:
            value = Config[host].getboolean(str(cle))
        except ValueError:
            value = Config[host][str(cle)]
    except KeyError:
        # print("Key Not Found.")
        value = None
    finally:
        lock.release()

    return value

def set_config(cle, valeur):
    """
    Set a configuration value according to a key.

    :cle: Key of the value
    :type cle: str
    :valeur: Value to set
    :type valeur: str or int
    :return: Operation success (True or False)
    :rtype: bool
    """

    # Check for value type
    if type(valeur) != str and type(valeur) != int:
        return False

    lock.acquire()
    try:
        if not cle in Config["Global"]:
            return False
        else:
            # CONFIGURATION[cle] = valeur
            Config.set("Global", str(cle), str(valeur))
    finally:
        lock.release()
    
    return True

def main():
    """
    get_config() tests
    ==================
    Valid key one thread
    Valid key 3 threads
    Valid key 30 threads
    Invalid key one thread
    Invalid key 10 threads

    set_config() tests
    ==================
    Valid key and value one thread
    Valid key and value 3 threads
    Valid key and value 30 threads
    Invalid key one thread
    Invalid key 10 threads
    Invalid value one thread
    Invalid value 10 threads
    """

    lire_configuration()

    # Test functions
    def test_get_config(key, attended_res):
        """
        Test function for get_config() function.

        :key: Key to pass
        :type key: str
        :attended_res: Attended result
        :type attended_res: str or int or None
        :return: Nothing
        :rtype: None
        """

        res = get_config(key)
        assert res == attended_res

        return None

    def test_set_config(key, value, attended_res):
        """
        Test function for get_config() function.

        :key: Key to pass
        :type key: str
        :value: Value to pass
        :type value: str or int
        :attended_res: Attended result
        :type attended_res: bool
        :return: Nothing
        :rtype: None
        """

        res = set_config(key, value)
        assert res == attended_res

        return None


    def test_threads_get(key, attended_res, loop_count):
        """
        Function to launch multiple threads at once.

        :key: Key to pass
        :type key: str
        :attended_result: Attended result
        :type attended_result: str or int or None
        :loop_count: Number of threads to run
        :type loop_count: int
        :return: OK status (True or False)
        :rtype: bool
        """

        try:
            myThread = []
            for i in range(0, loop_count):
                myThread.append(threading.Thread(target=test_get_config, args=(key, attended_result)))
                myThread[i].start()
            for i in range(0, len(myThread)):
                myThread[i].join()
        except(AssertionError, OSError, KeyError) as e:
            print(e)
            return False
        return True

    def test_threads_set(key, value, attended_res, loop_count):
        """
        Function to launch multiple threads at once.

        :key: Key to pass
        :type key: str
        :value: Value to pass
        :type value: str or int
        :attended_result: Attended result
        :type attended_result: str or int or None
        :loop_count: Number of threads to run
        :type loop_count: int
        :return: OK status (True or False)
        :rtype: bool
        """

        try:
            myThread = []
            for i in range(0, loop_count):
                if type(value) == int:
                    value += 1
                myThread.append(threading.Thread(target=test_set_config, args=(key, value, attended_result)))
                myThread[i].start()
            for i in range(0, len(myThread)):
                myThread[i].join()
            test_get_config(key, value)
        except(AssertionError, OSError, KeyError) as e:
            return False
        return True


    print("get_config() function tests")
    print("===========================")

    key = "Hote"
    attended_result = ""
    loop_count = 1
    res = test_threads_get(key, attended_result, loop_count)
    assert res == True
    print("Valid key one thread [OK]")

    key = "Port"
    attended_result = 8000
    loop_count = 3
    res = test_threads_get(key, attended_result, loop_count)
    assert res == True
    print("Valid key 3 threads [OK]")

    key = "Rep_servi"
    attended_result = "html/"
    loop_count = 30
    res = test_threads_get(key, attended_result, loop_count)
    assert res == True
    print("Valid key 30 threads [OK]")

    key = "Host"
    attended_result = None
    loop_count = 1
    res = test_threads_get(key, attended_result, loop_count)
    assert res == True
    print("Invalid key one thread [OK]")

    key = "Porte"
    attended_result = None
    loop_count = 10
    res = test_threads_get(key, attended_result, loop_count)
    assert res == True
    print("Invalid key 10 threads [OK]")

    print("Function tests passed!\n")


    print("set_config() function tests")
    print("===========================")

    key = "Hote"
    value = "localhost"
    attended_result = True
    loop_count = 1
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == True
    print("Valid key and value one thread [OK]")

    key = "Port"
    value = 9000
    attended_result = True
    loop_count = 3
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == True
    print("Valid key and value 3 threads [OK]")

    key = "Rep_servi"
    value = "/un_rep_quelconque"
    attended_result = True
    loop_count = 30
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == True
    print("Valid key and value 30 threads [OK]")

    key = "Host"
    value = "localhost"
    attended_result = False
    loop_count = 1
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == False
    print("Invalid key one thread [OK]")

    key = "Porte"
    value = 9000
    attended_result = False
    loop_count = 10
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == False
    print("Invalid key 10 threads [OK]")

    key = "Hote"
    value = ("quack", "hey")
    attended_result = False
    loop_count = 1
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == False
    print("Invalid value one thread [OK]")

    key = "Port"
    value = None
    attended_result = False
    loop_count = 10
    res = test_threads_set(key, value, attended_result, loop_count)
    assert res == False
    print("Invalid value 10 threads [OK]")

    print("Function tests passed!\n")


    print("All tests are OK.")

if __name__ == "__main__":
    main()
