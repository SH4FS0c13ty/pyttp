# -*- coding: utf-8 -*-
#!/usr/bin/env python3
######################################################################
#                               PyTTP                                #
######################################################################
#           client_http.py is a part of the PyTTP project.           #
######################################################################
# client_http.py is used to handle new clients connections.          #
######################################################################
# Shell syntax: client_http.py                                       #
######################################################################
# Author: Adrien CL                                                  #
# Copyright: Copyright 2020, PyTTP                                   #
# Credits: [Adrien CL, E.Würbel]                                     #
# License: MIT License (https://opensource.org/licenses/MIT)         #
# Version: 1.0.0                                                     #
# Maintainer: Adrien CL                                              #
# Email: adriencl@outlook.fr                                         #
######################################################################

"""
This is the connection handler of the PyTTP (Python HTTP Webserver).
This module is necessary to communicate with the client.

:Authors: Adrien CL
"""

# System modules
import socket, string, time, urllib.parse, os, mimetypes

# Internal modules
import config_srv

def traite_client(sock_client, addr):
    """
    Process the client connection.

    :sock_client: Opened client socket
    :type sock_client: socket.socket
    :addr: Client address
    :type addr: tuple
    :return: Nothing
    :rtype: None
    """

    # HTTP status codes
    http_status_codes = {
        200: "200 OK",
        400: "400 BAD REQUEST",
        404: "404 NOT FOUND",
        405: "405 METHOD NOT ALLOWED",
        500: "500 INTERNAL SERVER ERROR",
        505: "505 HTTP VERSION NOT SUPPORTED"
        }

    print("Processing client ...")
    try:
        req = lecture_requete(sock_client) # Read request
    except OSError:
        return None
    http_code = verifie_requete(req) # Verify request and store HTTP code

    # Set current host to None
    host = None
    
    # Generate response according to HTTP status code
    if http_code == 200:
        # Get host value
        for x in req.split("\r\n"):
            if x.startswith("Host:"):
                try:
                    host = x.split(":")[1].lstrip(" ")
                except IndexError:
                    pass
        file_path = construit_chemin_fichier(req.split("\r\n")[0], host)
        (header, data) = lecture_donnees(file_path)
    else:
        data = genere_donnees_erreur(http_code)
        size = len(data)
        header = genere_entete(http_code, type_contenu(None), size)

    try:
        print(req.split("\r\n")[0])
        print("Host:", str(host))
        print("HTTP Status Code:", http_status_codes[int(header.split(" ")[1])])
    except (ValueError, KeyError):
        pass
    log_req(req, header.split(" ")[1], addr) # Log the request

    # Assemble and send response
    response = header.encode("utf-8") + data
    try:
        sock_client.sendall(response)
    except socket.error as e:
        print("An error occured when trying to send data.")
        print(e)
        log_req("An error occured when trying to send data: " + str(e), 499, "localhost")

    return None

def log_req(req, code, host):
    """
    Log the request and http status code with timestamp
    
    :req: Received request
    :type req: str
    :code: HTTP status code
    :type code: int
    :host: Client host
    :type host: str
    :return: Nothing
    rtype: None
    """

    log_request_file = "logs/requests.log"

    # Check if HTTP status code is an integer
    if type(code) != int:
        try:
            code = int(code)
        except ValueError:
            return None

    # Check if log file exists
    if os.path.isfile("logs/requests.log"):
        mode = "a"
    else:
        mode = "w"

    # Create subfolder if non existant
    if not os.path.isdir(os.path.dirname(log_request_file)):
        os.makedirs(os.path.dirname(log_request_file), exist_ok=True)

    # Write log in a 'readable' way
    with open(log_request_file, mode) as log_opened:
        log_opened.write(time.strftime("[%d/%m/%Y] [%H:%M:%S]") + " [HTTP STATUS " + str(code) + "]\n")
        log_opened.write("Connection from " + str(host) + "\n")
        tmp_list = list(req.split("\r\n"))
        req = ""
        for x in tmp_list:
            req += x + "\\r\\n"
        log_opened.write(req + "\n\n")

    return None

def lecture_requete(sock_client):
    """
    Read the request.

    :sock_client: Client socket
    :type sock_client: socket.socket
    :return: Full request
    :rtype: str
    """

    data = b''
    buf = b''
    stop = False
    while not stop:
        buf = sock_client.recv(1024)
        data += buf
        if data.endswith(b'\r\n\r\n'):
            stop = True

    sock_client.shutdown(socket.SHUT_RD)

    return data.decode("utf-8")

def verifie_requete(req):
    """
    Verify the submitted request.

    :request: Request to analyze
    :type request: str
    :return: HTTP Error code
    :rtype: int
    """

    flag = True # Initialized final flag as True, will be changed to false if errors are detected
    req_list = req.split("\n") # Split the request to analyze every lines

    # Analyzing first line of request
    fline_list = req_list[0].split(" ")
    if len(fline_list) != 3:
        return 400 # Bad request
    elif fline_list[0] != "GET":
        return 405 # Method not allowed
    elif fline_list[2] != "HTTP/1.0\r" and fline_list[2] != "HTTP/1.1\r":
        # Check if this can be a valid HTTP version even if not supported
        if fline_list[2].startswith("HTTP/"):
            try:
                if not fline_list[2].endswith("\r"):
                    return 400
                http_version_number = float(fline_list[2].split("/")[1].rstrip("\r"))
                return 505 # HTTP version not supported
            except ValueError:
                return 400 # Bad request (Invalid HTTP version)
        else:
            return 400
    else:
        # Check every lines for malformed expression
        flag = False
        for x in req_list[1:]:
            if x == "\r":
                return 200 # OK
            try:
                # Check for Host field
                if x.startswith("Host:"):
                    # Check if host has empty value
                    try:
                        x = x.split(":")[1].lstrip(" ").rstrip("\r")
                    except IndexError:
                        x = None
                    res_check_host = config_srv.get_config("Rep_servi", x)
                    if res_check_host == None:
                        return 400 # Return 400 because Host field is invalid, if x is None and error 400 is returned, there is a configuration problem
                    flag = True
                else:
                    x = x.split(":")[0] # Store only the left part before the space
            except IndexError:
                return 400
            if not flag:
                for y in x:
                    if y not in string.ascii_letters + "-":
                        return 400
            flag = False
        return 400 # Bad request (Unknown error in request)

def genere_entete(code_reponse, type_mime, taille = None):
    """
    Generate an HTTP response header.
    
    :code_reponse: HTTP status code
    :type code_reponse: int
    :taille: Content size
    :type taille: int or None if unknown
    :return: Generated HTTP response header
    :rtype: str
    """

    # HTTP status codes
    http_status_codes = {
        200: "200 OK",
        400: "400 BAD REQUEST",
        404: "404 NOT FOUND",
        405: "405 METHOD NOT ALLOWED",
        500: "500 INTERNAL SERVER ERROR",
        505: "505 HTTP VERSION NOT SUPPORTED"
        }

    # Checking HTTP status code values
    if type(code_reponse) != int or code_reponse not in http_status_codes:
        code_reponse = 500

    # Generate first header line
    header = "HTTP/1.1 " + http_status_codes[code_reponse] + "\r\n"

    # Generate second header line
    header += "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n"

    # Generate third header line
    header += "Server: PyTTP" + "\r\n"
    
    # Generate fourth header line
    header += "Connection: close" + "\r\n"

    # Generate fifth header line
    header += type_mime + "\r\n"

    # Generate sixth header line if needed
    if type(taille) == int:
        header += "Content-Length: " + str(taille) + "\r\n"

    # Add the extra line to indicate the header endswith
    header += "\r\n"

    return header

def genere_donnees_erreur(code):
    """
    Generate error pages to send.
    
    :code: HTTP error code
    :type code: int
    :return: Error page in binary string
    :rtype: str
    """

    # HTML page structure
    html_page_base1 = b'<html><head><title>'
    html_page_base2 = b'</title></head><body><center><h1>'
    html_page_base3 = b'</h1></center><hr><center>PyTTP/1.0.0</center></body></html>'
    
    # HTTP error codes and pages
    http_status_codes = {
        400: b'400 Bad Request',
        404: b'404 Not Found',
        405: b'405 Method Not Allowed',
        500: b'500 Internal Server Error',
        505: b'505 HTTP Version Not Supported'
        }

    # Checking HTTP status code values
    if type(code) != int or code not in http_status_codes:
        code = 500

    # Generate full error page
    http_error_page = html_page_base1 + http_status_codes[code] + html_page_base2 + http_status_codes[code] + html_page_base3

    return http_error_page 

def construit_chemin_fichier(ligne1_entete, host=None):
    """
    Build file path using the first header line.
    
    :ligne1_entete: First header line
    :type ligne1_entete: str
    :host: Host value
    :type host: str
    :return: Path of the requested file
    :rtype: str
    """

    # Split the first line
    line_list = ligne1_entete.split(" ")

    # Cleaning the requested path
    req_path_clean = urllib.parse.unquote(line_list[1].split("?")[0])

    # Check if index.html requested
    if req_path_clean.endswith("/"):
        req_path_clean += "index.html"

    # Creating the full path
    host_res = config_srv.get_config("Rep_servi", host)
    if host_res != None:
        req_path = host_res + req_path_clean
    else:
        req_path = "/" # Will never be a file so an error 404 will be throwned afterward

    return req_path

def lecture_donnees(nom_fichier):
    """
    Read the requested data and generate an appropriate response.
    
    :nom_fichier: File to read
    :type nom_fichier: str
    :return: A tuple containing the header and data
    :rtype: tuple
    """

    # Check if the resource exists, otherwise return 404
    if os.path.isfile(nom_fichier):
        # Try to open file, return 500 otherwise
        try:
            # Check if the requested resource uses "../", could be a critical vulnerability
            if nom_fichier.find("../") != -1 or nom_fichier.find("..\\") != -1:
                raise OSError
            with open(nom_fichier, "rb") as opened_file: # Open file in read bytes mode
                # Data to send (read from the file)
                data = opened_file.read()
                size = len(data) # Content-Length
                header = genere_entete(200, type_contenu(nom_fichier), size) # Header generation
        except OSError:
            data = genere_donnees_erreur(500) # Data to send (generated error page)
            size = len(data)
            header = genere_entete(500, type_contenu(None), size)
    else:
        data = genere_donnees_erreur(404)
        size = len(data)
        header = genere_entete(404, type_contenu(None), size)

    return (header, data)

def type_contenu(fichier=None):
    """
    Detect the MIME type of a submitted file.

    :fichier: File to check
    :type fichier: str
    :return: MIME type formatted as an HTTP Content-Type values, False is file not found
    :rtype: str or bool
    """

    # Check if the file exists
    if fichier == None:
        return "Content-Type: text/html; charset=UTF-8"
    elif not os.path.isfile(fichier):
        return False

    # Detect MIME type and encoding
    (file_type, file_encoding) = mimetypes.guess_type(fichier)

    # Check if not able to find MIME type
    if file_type == None:
        file_type = "application/octet-stream"
        file_encoding = None

    # Use charset=UTF-8 for text/html type
    if file_type == "text/html":
        file_encoding = "UTF-8"

    # Format data
    form = "Content-Type: "+ file_type
    if file_encoding != None:
        form +=  "; charset=" + file_encoding

    return form


def main():
    """
    verifie_requete() tests
    =======================
    Valid GET request (code 200)
    Valid POST request (error 405)
    Valid GET request HTTP/2.0 (error 505)
    Invalid GET request (2 elements in first line) (error 400)
    Invalid GET request (4 elements in first line) (error 400)
    Invalid GET request (Bad first part form, invalid characters) (error 400)
    Invalid GET request (Bad first part form, ":" not present) (error 400)
    Valid GET request (Extra line before last line of request) (code 200)

    genere_entete() tests
    =====================
    Valid HTTP status code with size
    Valid HTTP status code without size
    Invalid HTTP status code with size
    Invalid HTTP status code without size
    Invalid HTTP status code (str) with invalid size (str)

    construit_chemin_fichier() tests
    ================================
    /!\ No invalid header will be tested because the full request will be validated before. /!\
    Valid first header line
    Valid first header line with ? path
    Valid first header line with encoded characters
    Valid first header line ending with /

    lecture_donnees() tests
    =======================
    /!\ No invalid type will be tested because they will be automatically converted to string before. /!\
    Valid file
    Valid file without permissions
    Invalid file (inexistant)
    Valid path
    Invalid path (inexistant)
    Invalid path (use of "../")

    type_contenu() tests
    ====================
    Valid TXT file
    Valid HTML file
    Valid PNG file
    Valid MP4 file
    Valid MKV file
    Valid ZIP file
    Invalid file (non-existent)
    Valid file unknown type (.dat)
    No file (for error page)
    """

    config_srv.lire_configuration()

    print("verifie_requete() function tests")
    print("================================")

    request = 'GET / HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 200
    print("Valid GET request (code 200) [OK]")

    request = 'GET /test/index.html HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 200
    print("Valid GET request with different path (code 200) [OK]")

    request = 'POST / HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 405
    print("Valid POST request (error 405) [OK]")

    request = 'GET / HTTP/2.0\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 505
    print("Valid GET request HTTP/2.0 (error 505) [OK]")

    request = 'GET / NOT_HTTP\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 400
    print("Invalid GET request (Incorrect HTTP version) (error 400) [OK]")

    request = 'GET HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 400
    print("Invalid GET request (2 elements in first line) (error 400) [OK]")

    request = 'GET / HTTP/1.1 ANOTHER_STRING\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 400
    print("Invalid GET request (4 elements in first line) (error 400) [OK]")

    request = 'GET / HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccèpt: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 400
    print("Invalid GET request (Bad first part form, invalid characters) (error 400) [OK]")

    request = 'GET / HTTP/1.1\r\nHost: \r\nUser-Agent ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 400
    print("Invalid GET request (Bad first part form, \":\" not present) (error 400) [OK]")

    request = 'GET / HTTP/1.1\r\nHost: \r\nUser-Agent: ELinks/0.12pre6 (textmode; Linux 4.15.0-88-generic x86_64; 120x30-2)\r\n\r\nAccept: */*\r\nAccept-Language: en\r\nConnection: Keep-Alive\r\n\r\n'
    res = verifie_requete(request)
    assert res == 200
    print("Valid GET request (Extra line before last line of request) (code 200) [OK]")

    print("Function tests passed!\n")


    print("genere_entete() function tests")
    print("==============================")

    http_status_codes = {
        200: "200 OK",
        400: "400 BAD REQUEST",
        404: "404 NOT FOUND",
        405: "405 METHOD NOT ALLOWED",
        500: "500 INTERNAL SERVER ERROR",
        505: "505 HTTP VERSION NOT SUPPORTED"
        }

    code = 200
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 200 with valid size [OK]")

    code = 400
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 400 with valid size [OK]")

    code = 404
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 404 with valid size [OK]")

    code = 405
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 405 with valid size [OK]")

    code = 500
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 500 with valid size [OK]")

    code = 505
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 505 with valid size [OK]")

    code = 200
    size = None
    attended_response = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Valid HTTP status code 200 without size [OK]")

    code = -1
    attended_code = 500
    size = 280
    attended_response = "HTTP/1.1 " + http_status_codes[attended_code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Invalid HTTP status code with valid size [OK]")

    code = 764
    attended_code = 500
    size = None
    attended_response = "HTTP/1.1 " + http_status_codes[attended_code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Invalid HTTP status code without valid size [OK]")

    code = "bruh"
    attended_code = 500
    size = "nonononono"
    attended_response = "HTTP/1.1 " + http_status_codes[attended_code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "\r\n"
    res = genere_entete(code, type_contenu(None), size)
    assert res == attended_response
    print("Invalid HTTP status code with invalid size [OK]")

    print("Function tests passed!\n")


    # /!\ No invalid header will be tested because the full request will be validated before. /!\

    print("construit_chemin_fichier() function tests")
    print("=========================================")


    first_header_line = "GET /path/yes.html HTTP/1.1\r\n\r\n"
    attended_result = config_srv.get_config("Rep_servi") + "/path/yes.html"
    res_code = verifie_requete(first_header_line)
    if res_code == 200:
        res = construit_chemin_fichier(first_header_line)
    else:
        res = None
    assert res == attended_result
    print("Valid first header line [OK]")

    first_header_line = "GET /actuel/news.html?ordre=1 HTTP/1.1\r\n\r\n"
    attended_result = config_srv.get_config("Rep_servi") + "/actuel/news.html"
    res_code = verifie_requete(first_header_line)
    if res_code == 200:
        res = construit_chemin_fichier(first_header_line)
    else:
        res = None
    assert res == attended_result
    print("Valid first header line with ? path [OK]")

    first_header_line = "GET /doc/doc%20en%20fran%C3%A7ais/doc.html HTTP/1.1\r\n\r\n"
    attended_result = config_srv.get_config("Rep_servi") + "/doc/doc en français/doc.html"
    res_code = verifie_requete(first_header_line)
    if res_code == 200:
        res = construit_chemin_fichier(first_header_line)
    else:
        res = None
    assert res == attended_result
    print("Valid first header line with encoded characters [OK]")

    first_header_line = "GET / HTTP/1.1\r\n\r\n"
    attended_result = config_srv.get_config("Rep_servi") + "/index.html"
    res_code = verifie_requete(first_header_line)
    if res_code == 200:
        res = construit_chemin_fichier(first_header_line)
    else:
        res = None
    assert res == attended_result
    print("Valid first header line ending with / [OK]")

    print("Function tests passed!\n")


    # /!\ No invalid type will be tested because they will be automatically converted to string before. /!\

    print("lecture_donnees() function tests")
    print("================================")


    first_request_line = "GET /index.html HTTP/1.1"
    code = 200
    file_path = construit_chemin_fichier(first_request_line)
    size = os.path.getsize(file_path)
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = b'<!DOCTYPE HTML>\r\n<html>\r\n    <head>\r\n        <title>Test</title>\r\n    </head>\r\n    <body>\r\n        <div>\r\n            This is an HTML test page.\r\n        </div>\r\n    </body>\r\n</html>'
    try:
        assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print("An error occured while testing, the time may be different from 1 second and triggers the update.")
        print(e)
        print("Below are the generated header and the attended header:")
        print(header)
        print(attended_response_header)
    print("Valid file [OK]")

    first_request_line = "GET /no_perms.html HTTP/1.1"
    code = 500
    file_path = construit_chemin_fichier(first_request_line)
    size = len(genere_donnees_erreur(code))
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = genere_donnees_erreur(code)
    try:
        assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print(e)
        print("WARNING: The file is not synced due to denied permissions, please recreate it if it does not exist.")
    print("Valid file without permissions [OK]")

    first_request_line = "GET /inexistant.html HTTP/1.1"
    code = 404
    file_path = construit_chemin_fichier(first_request_line)
    size = len(genere_donnees_erreur(code))
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = genere_donnees_erreur(code)
    try:
        assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print("An error occured while testing, the time may be different from 1 second and triggers the update.")
        print(e)
        print("Below are the generated header and the attended header:")
        print(header)
        print(attended_response_header)
    print("Invalid file (inexistant) [OK]")

    first_request_line = "GET /more/ HTTP/1.1"
    code = 200
    file_path = construit_chemin_fichier(first_request_line)
    size = os.path.getsize(file_path)
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = b'<!DOCTYPE HTML>\r\n<html>\r\n    <head>\r\n        <title>Test</title>\r\n    </head>\r\n    <body>\r\n        <div>\r\n            This is an HTML test page.\r\n        </div>\r\n    </body>\r\n</html>'
    try:
        assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print("An error occured while testing, the time may be different from 1 second and triggers the update.")
        print(e)
        print("Below are the generated header and the attended header:")
        print(header)
        print(attended_response_header)
    print("Valid path [OK]")

    first_request_line = "GET /blabla/ HTTP/1.1"
    code = 404
    file_path = construit_chemin_fichier(first_request_line)
    size = len(genere_donnees_erreur(code))
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = genere_donnees_erreur(code)
    try:
        assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print("An error occured while testing, the time may be different from 1 second and triggers the update.")
        print(e)
        print("Below are the generated header and the attended header:")
        print(header)
        print(attended_response_header)
    print("Invalid path (inexistant) [OK]")

    first_request_line = "GET /../cmd_here.bat HTTP/1.1"
    code = 500
    file_path = construit_chemin_fichier(first_request_line)
    size = len(genere_donnees_erreur(code))
    attended_response_header = "HTTP/1.1 " + http_status_codes[code] + "\r\n" + "Date: " + time.strftime("%a, %d %b. %Y %H:%M:%S") + " CET" + "\r\n" + "Server: PyTTP" + "\r\n" + "Connection: close" + "\r\n" + "Content-Type: " + "text/html; charset=UTF-8" + "\r\n" + "Content-Length: " + str(size) + "\r\n" + "\r\n"
    (header, data) = lecture_donnees(file_path)
    attended_response_data = genere_donnees_erreur(code)
    try:
      assert header == attended_response_header and data == attended_response_data
    except AssertionError as e:
        print(e)
        print("WARNING: The file is not synced due to denied permissions, please recreate it if it does not exist.")
    print("Invalid path (use of \"../\") [OK]")

    print("Function tests passed!\n")


    print("type_contenu() function tests")
    print("=============================")

    file = "html/tests/test.txt"
    attended_result = "Content-Type: text/plain"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid TXT file [OK]")

    file = "html/tests/test.html"
    attended_result = "Content-Type: text/html; charset=UTF-8"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid HTML file [OK]")

    file = "html/tests/test.png"
    attended_result = "Content-Type: image/png"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid PNG file [OK]")

    file = "html/tests/test.mp4"
    attended_result = "Content-Type: video/mp4"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid MP4 file [OK]")

    file = "html/tests/test.mkv"
    attended_result = "Content-Type: video/x-matroska"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid MKV file [OK]")

    file = "html/tests/test.zip"
    attended_result = "Content-Type: application/x-zip-compressed"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid ZIP file [OK]")

    file = "html/tests/non_existant.txt"
    attended_result = False
    res = type_contenu(file)
    assert res == attended_result
    print("Invalid file (non-existent) [OK]")

    file = "html/tests/test.dat"
    attended_result = "Content-Type: application/octet-stream"
    res = type_contenu(file)
    assert res == attended_result
    print("Valid file unknown type (.dat) [OK]")

    file = None
    attended_result = "Content-Type: text/html; charset=UTF-8"
    res = type_contenu(file)
    assert res == attended_result
    print("No file (for error pages) [OK]")

    print("Function tests passed!\n")


    print("All tests are OK.")


if __name__ == "__main__":
    main()
